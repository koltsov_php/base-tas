<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

/**
 * Transliterator Interface
 */
interface TransliteratorInterface
{
    /**
     * Transliterate text
     *
     * @param string $textToTransliterate Text to transliterate
     */
    public static function transliterate($textToTransliterate);
}
