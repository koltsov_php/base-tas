<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

use Symfony\Component\HttpFoundation\File\File;

class FileHelper
{
    const SYSTEM_TMP_DIR = '/tmp/';
    /**
     * @param string $string
     *
     * @return string
     */
    public static function generateName($string)
    {
        $result = strtolower(Transliterator::ruToEn(trim($string)));
        $result = str_replace(array(' '), array('_'), $result);
        $result = preg_replace('/[^a-z0-9_]/i', '_', $result);
        $result = preg_replace('/^_|_*/', '', $result);
        $result = preg_replace('/_*$/', '', $result);
        $result = preg_replace("/_{2,}/", "_", $result);
        $result = trim($result, '_');

        return $result;
    }

    /**
     * @param string $contentType
     *
     * @return string
     */
    public static function getExtensionByContentType($contentType)
    {
        $parts = explode('/', $contentType);
        return !empty($parts[1]) && $parts[1] == 'jpeg' ? 'jpg' : @$parts[1];
    }

    /**
     * @param $content
     * @return File
     */
    public static function createTempFileFromContent($content)
    {
        $filePath = tempnam(self::SYSTEM_TMP_DIR, '');
        $fp = fopen($filePath, 'w');
        fwrite($fp, $content);
        fclose($fp);

        return new File($filePath);
    }
}