<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

class Transliterator
{
    const DEFAULT_SEPARATOR = '-';

    /**
     * Transliterate Russian text to English
     *
     * @param string $russianText Russian text
     *
     * @return string
     */
    public static function ruToEn(string $russianText): string
    {
        return RussianToEnglish::transliterate($russianText);
    }

    /**
     *
     * Transliterate Russian text to English
     * @param string $russianText
     * @param string $separator
     * @return string
     */
    public static function ruToCode(string $russianText, string $separator = self::DEFAULT_SEPARATOR): string
    {
        return mb_strtolower(preg_replace('/[^a-z0-9]+/iu', $separator, RussianToEnglish::transliterate($russianText)));
    }
}
