<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

class StringHelper
{
    /**
     * @param string $str
     * @param string $enc
     * @return string
     */
    public static function mb_ucfirst(string $str, $enc = 'UTF-8'): string
    {
        return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc) . mb_substr($str, 1, mb_strlen($str, $enc), $enc);
    }

    /**
     * @param $code
     * @return mixed
     */
    public static function getUnderscoreCodeFromHyphen($code)
    {
        return str_replace("-", "_", $code);
    }

    /**
     * @param $code
     * @return mixed
     */
    public static function getHyphenCodeFromUnderscore($code)
    {
        return str_replace("_", "-", $code);
    }

    /**
     * @param $string
     * @param string $separator
     * @return mixed|string
     */
    public static function generateSemanticName($string, $separator = '-')
    {
        $result = strtolower(Transliterator::ruToEn(trim($string)));
        $result = str_replace('+', 'plus', $result);
        $result = preg_replace('/[^a-z0-9\-]/i', $separator, $result);
        $result = preg_replace("/".$separator."{2,}/", $separator, $result);
        $result = trim($result, $separator);

        return $result;
    }

    /**
     * @param $string
     * @return mixed
     */
    public static function strToElasticHexKeyword($string)
    {
        $hexstr = unpack('H*', mb_strtolower($string));
        return array_shift($hexstr);
    }

    /**
     * @param string $phone
     * @return string
     */
    public static function clearPhone(string $phone): string
    {
        return preg_replace('/[()\s-\+]/', '', $phone);
    }

    /**
     * @param string $value
     * @return string
     */
    public static function getXssClearValue(string $value): string
    {
        return htmlspecialchars(strip_tags($value));
    }
}
