<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

class RussianToEnglish implements TransliteratorInterface
{
    /**
     * @var array Rules of transliteration from Ukrainian to English
     */
    private static $russianToEnglishRules = [
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'E',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'Y',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'Kh',
        'Ц' => 'Ts',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Shch',
        'Ъ' => '',
        'Ы' => 'Y',
        'Ь' => '',
        'Э' => 'e',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'kh',
        'ц' => 'ts',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ъ'  => '',
        'ы'  => 'y',
        'ь'  => '',
        'э' => 'e',
        'ю' => 'iu',
        'я' => 'ia',
        '\'' => '',
        "ї" => "yi", "і" => "i", "є" => "ye", "ґ" => "g"
    ];

    /**
     * Transliterate Russian text to English
     *
     * @param string $russianText Russian text
     *
     * @return string
     */
    public static function transliterate($russianText)
    {
        $transliteratedText = '';

        if (mb_strlen($russianText) > 0) {
            // If found "Zgh|zgh" exception then replace it
            if (self::checkForZghException($russianText)) {
                $russianText = str_replace(['Зг', 'зг'], ['Zgh', 'zgh'], $russianText);
            }
            // Transliteration is doing by rendering each letter
            $transliteratedText = str_replace(
                array_keys(self::$russianToEnglishRules),
                array_values(self::$russianToEnglishRules),
                $russianText
            );
        }

        return trim($transliteratedText);
    }

    /**
     * Check Russian text for "Zgh|zgh" exception
     *
     * @param string $russianText Russian text
     *
     * @return bool
     */
    private static function checkForZghException($russianText)
    {
        return (bool)mb_substr_count($russianText, 'Зг') || (bool)mb_substr_count($russianText, 'зг');
    }
}
