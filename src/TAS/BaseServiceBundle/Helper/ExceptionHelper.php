<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Helper;

class ExceptionHelper
{
    /**
     * @param \Exception $e
     * @return array
     */
    public static function getLoggerArrayFromException(\Exception $e): array
    {
        return [
            'MESSAGE' => $e->getMessage(),
            'FILE' => $e->getFile(),
            'LINE' => $e->getLine(),
            'TRACE' => $e->getTraceAsString(),
            'CODE' => $e->getCode()
        ];
    }
}
