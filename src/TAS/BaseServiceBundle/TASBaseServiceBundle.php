<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TASBaseServiceBundle extends Bundle
{
}
