<?php declare(strict_types = 1);
namespace TAS\BaseServiceBundle\Service;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use JMS\Serializer\Serializer;

abstract class AbstractHttpService
{
    /** @var HttpClient */
    protected $_httpClient;

    /** @var Serializer */
    protected $_serializer;

    /** @var int */
    protected $_lastRequestStatus;

    /**
     * @param string $uri
     * @param array $params
     * @param string $returnType
     * @param string $format
     * @param array $options
     * @return object
     */
    public function getRequest(string $uri, array $params, string $returnType, string $format, array $options = [])
    {
        try {
            $response = $this->_httpClient->get($uri . (empty($params) ? '' : '?' . http_build_query($params)), $options);
        } catch (ClientException $e) {
            $this->_lastRequestStatus = $e->getResponse()->getStatusCode();
            return null;
        }

        $this->_lastRequestStatus = $response->getStatusCode();

        if ($returnType) {
            return $this->_serializer->deserialize($response->getBody(), $returnType, $format);
        }

        return null;
    }

    /**
     * @param string $uri
     * @param $request
     * @param string $returnType
     * @param string $format
     * @param array $options
     * @return mixed
     */
    public function postRequest(string $uri, $request, string $returnType, string $format, array $options = [])
    {
        try {
            $path = $this->_httpClient->getConfig('base_uri')->getPath();
            $response = $this->_httpClient->post(
                $path.$uri, array_merge($options, [
                'body' => $this->_serializer->serialize($request, $format)
            ]));
        } catch (ClientException $e) {
            $this->_lastRequestStatus = $e->getResponse()->getStatusCode();
            return null;
        } catch (ServerException $e) {
            $this->_lastRequestStatus = $e->getResponse()->getStatusCode();
            return null;
        }

        $this->_lastRequestStatus = $response->getStatusCode();

        if ($returnType) {
            return $this->_serializer->deserialize($response->getBody(), $returnType, $format);
        }

        return null;
    }


    /**
     * @param string $uri
     * @param $request
     * @param string $returnType
     * @param string $format
     * @param array $options
     * @return object
     */
    public function putRequest(string $uri, $request, string $returnType, string $format, array $options = [])
    {
        try {
            $response = $this->_httpClient->put($uri, array_merge($options, [
                'body' => $this->_serializer->serialize($request, $format)
            ]));
        } catch (ClientException $e) {
            $this->_lastRequestStatus = $e->getResponse()->getStatusCode();
            return null;
        } catch (ServerException $e) {
            $this->_lastRequestStatus = $e->getResponse()->getStatusCode();
            return null;
        }

        $this->_lastRequestStatus = $response->getStatusCode();

        if ($returnType) {
            return $this->_serializer->deserialize($response->getBody(), $returnType, $format);
        }

        return null;
    }

    /**
     * @return int
     */
    public function getLastRequestStatus()
    {
        return $this->_lastRequestStatus;
    }
}
