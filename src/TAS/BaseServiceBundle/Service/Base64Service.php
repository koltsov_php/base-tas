<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Service;

class Base64Service
{
    /** @var string */
    private $data;

    /** @var string */
    private $contentType;

    /** @var string */
    private $content;
    /** @var array */
    private $binaryImagePrefix = [
        'JPEG' => '\xFF\xD8\xFF',
        'PNG'  => '\x89\x50\x4e\x47\x0d\x0a\x1a\x0a',
        'GIF'  => '\x47\x49\x46\x38'
    ];
    /**
     * Base64Service constructor.
     * @param string $data
     * @throws \Exception
     */
    public function __construct(string $data)
    {
        $this->data = $data;

        $this->init();
    }

    /**@deprecated use constructor
     * @param $data
     *
     * @return bool
     */
    public static function validateBase64Data($data)
    {
        return (bool)preg_match('/^data:(.*);base64,/Ui', $data);
    }

    /**
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getDecodedContent()
    {
        return base64_decode($this->content);
    }

    /**
     * @throws \Exception
     */
    private function init()
    {
        preg_match('/^data:(.*);base64,/Ui', $this->data, $result);

        if (empty($result[1])) {
            throw new \Exception('Incorrect Base64 content');
        }

        $this->contentType = $result[1];
        $this->content = str_replace('data:' . $this->contentType . ';base64,', '', $this->data);
    }

    /**
     * @return bool
     */
    public function validateImage()
    {
        if (!preg_match('/^image\\/(png|jpg|jpeg|gif)$/i', $this->getContentType())) {
            return false;
        }

        if (!preg_match('/^('.implode('|', array_values($this->binaryImagePrefix)).')/i', $this->getDecodedContent())) {
            return false;
        }

        return true;
    }
}