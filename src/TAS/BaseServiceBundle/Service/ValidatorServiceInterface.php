<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Service;

interface ValidatorServiceInterface
{
    /**
     * @param $object
     * @param null $constraints
     * @param null $groups
     */
    public function validateAndThrow($object, $constraints = null, $groups = null);
}
