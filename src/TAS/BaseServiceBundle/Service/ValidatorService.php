<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Service;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidatorService implements ValidatorServiceInterface
{
    /** @var ValidatorInterface */
    private $_validator;

    /**
     * ValidatorHelper constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->_validator = $validator;
    }

    /**
     * @param $object
     * @param null $constraints
     * @param null $groups
     */
    public function validateAndThrow($object, $constraints = null, $groups = null)
    {
        $errors = $this->_validator->validate($object, $constraints, $groups);
        if (count($errors) > 0) {
            $errorsMessages = [];
            /** @var ConstraintViolationInterface $row */
            foreach ($errors as $row) {
                $errorsMessages[] = $row->getPropertyPath(). ': ' .$row->getMessage();
            }
            throw new BadRequestHttpException(implode(' | ', $errorsMessages));
        }
    }
}
