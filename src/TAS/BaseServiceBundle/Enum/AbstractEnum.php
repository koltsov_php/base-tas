<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Enum;

use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class AbstractEnum
{
    static protected $_enums;

    /** @var string */
    protected $_value;

    /**
     * @param $type
     */
    public function __construct($type)
    {
        if (array_search($type, static::$_enums, true) === false) {
            throw new BadRequestHttpException(sprintf("%s not in [%s]", $type, implode(',', self::getEnumsList())));
        }
        $this->_value = $type;
    }

    /**
     * @return array
     */
    public static function getEnumsList(): array
    {
        return array_values(static::$_enums);
    }

    /**
     * @return array
     */
    public static function getEnumsListIds(): array
    {
        return array_keys(static::$_enums);
    }

    /**
     * @param $id
     * @return string
     */
    public static function getValueById($id): string
    {
        return (string)@static::$_enums[$id];
    }

    /**
     * @param $value
     * @return false|int|string
     */
    public static function getIdByValue($value)
    {
        return array_search($value, static::$_enums, true);
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->_value;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return array_search($this->_value, static::$_enums, true);
    }
}
