<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Enum;

class MonthEnum extends AbstractEnum
{
    private const JANUARY = "January";
    private const FEBRUARY = "February";
    private const MARCH = "March";
    private const APRIL = "April";
    private const MAY = "May";
    private const JUNE = "June";
    private const JULY = "July";
    private const AUGUST = "August";
    private const SEPTEMBER = "September";
    private const OCTOBER = "October";
    private const NOVEMBER = "November";
    private const DECEMBER = "December";

    static protected $_enums = [
        1  => self::JANUARY,
        2  => self::FEBRUARY,
        3  => self::MARCH,
        4  => self::APRIL,
        5  => self::MAY,
        6  => self::JUNE,
        7  => self::JULY,
        8  => self::AUGUST,
        9  => self::SEPTEMBER,
        10 => self::OCTOBER,
        11 => self::NOVEMBER,
        12 => self::DECEMBER,
    ];

    static protected $_titles_ru = [
        self::JANUARY   => "Январь",
        self::FEBRUARY  => "Февраль",
        self::MARCH     => "Март",
        self::APRIL     => "Апрель",
        self::MAY       => "Май",
        self::JUNE      => "Июнь",
        self::JULY      => "Июль",
        self::AUGUST    => "Август",
        self::SEPTEMBER => "Сентябрь",
        self::OCTOBER   => "Октябрь",
        self::NOVEMBER  => "Ноябрь",
        self::DECEMBER  => "Декабрь",
    ];

    /**
     * @return array|self[]
     */
    public static function ALL(): array
    {
        $result = [];
        foreach (self::$_enums as $row){
            $result[] = new self($row);
        }

        return $result;
    }

    public function getTitleRu(): string
    {
        return self::$_titles_ru[$this->getValue()];
    }
}