<?php declare(strict_types=1);

namespace TAS\BaseServiceBundle\Test;

class RandomGeneratedValue
{
    const TTN_NUMBER = 59000261887045;
    const MAX_SIGNED_INT = 2147483647;
    /**
     * @return int
     */
    public static function getUniqueInteger() : int
    {
        return rand(10000, 99999);
    }

    /**
     * @return int
     */
    public static function getUniqueLargeInteger() : int
    {
        return self::MAX_SIGNED_INT - self::getUniqueInteger();
    }

    /**
     * @return int
     */
    public static function getUniqueSmallInteger() : int
    {
        return rand(1, 9);
    }

    /**
     * @return float
     */
    public static function getUniqueDouble() : float
    {
        return round(rand(101, 999999) / 100, 2);
    }

    /**
     * @return string
     */
    public static function getUniqueString() : string
    {
        return uniqid('str');
    }

    /**
     * @return string
     */
    public static function getUniqueYesNoNames() : string
    {
        $yesnoNames = array('Y', 'N');
        return $yesnoNames[rand(0, 1)];
    }

    /**
     * @return bool
     */
    public static function getUniqueBoolean() : bool
    {
        return rand(0, 1) == 1;
    }

    /**
     * @return \DateTime
     */
    public static function getUniqueDateTime() : \DateTime
    {
        return new \DateTime();
    }

    /**
     * @return float|int
     */
    public static function getUniqueGeoCoord()
    {
        return rand() / getrandmax();
    }

    /**
     * @return string
     */
    public static function getUniquePhone() : string
    {
        return '+380'.rand(111111111, 999999999);
    }

    /**
     * @param  int $length
     * @return string
     */
    public static function getUniqueStringWithLength(int $length) : string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    /**
     * @return string
     */
    public static function getUniqueStringWithoutNumber() : string
    {
        return preg_replace("/[0-9]/", '', RandomGeneratedValue::getUniqueString());
    }

    /**
     * @param $wordLength
     * @param $whitespacesCount
     * @return string
     */
    public static function getUniqueCompoundString(int $whitespacesCount, int $wordLength = 5) : string
    {
        $searchValue = self::getUniqueStringWithLength($whitespacesCount*$wordLength);
        return wordwrap($searchValue, (int)ceil(strlen($searchValue)/$whitespacesCount), ' ', true);
    }

    /**
     * @param $prefix
     * @return string
     */
    public static function getUniqueStringWithPrefix(string $prefix) : string
    {
        return $prefix.self::getUniqueString();
    }

    /**
     * @param array $array
     * @return mixed
     */
    public static function getUniqueValueFromArray(array  $array)
    {
        return $array[array_rand($array)];
    }

    /**
     * @return int
     */
    public static function getRandomTtn()
    {
        return self::TTN_NUMBER - RandomGeneratedValue::getUniqueLargeInteger();
    }
}
